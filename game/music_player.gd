extends StreamPlayer

var song_menu = preload("res://Music/RoccoW_-_Pumped.ogg")
var song_passive = preload("res://Music/RoccoW_-_Create_A_Song_in_A_Day_Challenge_1.ogg")
var song_active = preload("res://Music/RoccoW_-_Sea_Battles_in_Space.ogg")

var active_pos = null
var passive_pos = null

func _ready():
	pass


#always keep the current position (in time) of the passive and active songs and resume them when playing them again
#it sounds better this way!

func switch_to_menu():
	if(get_stream() != song_menu):
		if(get_stream() == song_passive):
			passive_pos = get_pos()
		if(get_stream() == song_active):
			active_pos = get_pos()
		set_stream(song_menu)
		play()


func switch_to_passive():
	if(get_stream() != song_passive):
		if(get_stream() == song_active):
			active_pos = get_pos()
		set_stream(song_passive)
		if(passive_pos != null):
			play(passive_pos)
		else:
			play()


func switch_to_active():
	if(get_stream() != song_active):
		if(get_stream() == song_passive):
			passive_pos = get_pos()
		set_stream(song_active)
		if(active_pos != null):
			play(active_pos)
		else:
			play()