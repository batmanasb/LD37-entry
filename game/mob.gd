extends KinematicBody2D

var speed = 40.0
var blazer_speed = 50.0
var crumble_enabled = false
var boulder_enabled = false
var algae_enabled = false
var timer = null
var ready_to_breed = false
var breeding_cooldown = 5.0
var seeds_remaining = 1

var vel = Vector2()
var player = null

onready var mobs = get_tree().get_root().get_node("Game/Mobs")

func _ready():
	set_fixed_process(true)
	
	#get a reference to player
	player = weakref(get_tree().get_root().get_node("Game/Player"))
	
	#point velocity in a random direction
	randomize()
	vel = Vector2(0,speed).rotated(rand_range(0,deg2rad(360)))

	if(algae_enabled):
		timer = Timer.new()
		timer.connect("timeout", self, "reset_breed_cooldown")
		timer.set_wait_time(breeding_cooldown)
		timer.start()
		add_child(timer)


func _fixed_process(delta):
	var remainder = move(vel*delta)
	
	if(is_colliding()):
		var body = get_collider()
		
		if(crumble_enabled and body.has_method("is_boulder")):
			if(body.is_boulder()):
				crumble()
				crumble_enabled = false
		elif(boulder_enabled and body.has_method("can_crumble")):
			if(body.can_crumble()):
				body.crumble()
				body.crumble_enabled = false
		elif(algae_enabled and body.has_method("is_algae")):
			if(body.is_algae() and ready_to_breed):
				body.ready_to_breed = false
				body.timer.start()
				breed()
		
		if(body.get_name() == "Line"):
			#get rid of collision to prevent an infinite loop of getting hit over and over
			body.set_collision_layer(0)
			
			#tell player that it got hit
			if(player.get_ref()):
				player.get_ref().hit()
		
		#reflect velocity on collision
		var normal = get_collision_normal()
		bounce(normal)
		
		#tell the other body to bounce as well
		if(body.has_method("bounce")):
			body.bounce(-normal)


func bounce(normal):
	vel = vel - 2 * vel.dot(normal) * normal


func is_boulder():
	return boulder_enabled


func can_crumble():
	return crumble_enabled


func is_algae():
	return algae_enabled


func crumble():
	set_layer_mask(0)
	var a = levels.mob_scene.instance()
	a.set_pos(get_pos())
	var b = levels.mob_scene.instance()
	b.set_pos(get_pos()+Vector2(0,5).rotated(rand_range(0, deg2rad(360))))
	mobs.add_child(a)
	mobs.add_child(b)
	queue_free()


func breed():
	ready_to_breed = false
	if(seeds_remaining > 0):
		seeds_remaining -= 1
		timer.start()
		var node = levels.algae_scene.instance()
		node.set_pos(get_pos()+Vector2(0,4).rotated(rand_range(0, deg2rad(360))))
		mobs.add_child(node)


func reset_breed_cooldown():
	ready_to_breed = true