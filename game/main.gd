extends Node2D

var grid_size = 100
var area_total = grid_size * grid_size
var area_filled = 0

var bar_tiles_filled = 0

onready var level = get_node("Level")
onready var boarder = get_node("Boarder")
onready var line = get_node("Line")
onready var player = get_node("Player")
onready var mobs = get_node("Mobs")
onready var anim = get_tree().get_root().get_node("Game/AnimationPlayer")

func _ready():
	#start game
	load_map()

func initialize_map():
	#position player
	player.set_pos(Vector2(101, 200-3))
	
	#draw boarder around map
	for i in range(grid_size):
		boarder.set_cell(i, 0, tiles.BOARDER)
		boarder.set_cell(i, grid_size-1, tiles.BOARDER)
		boarder.set_cell(0, i, tiles.BOARDER)
		boarder.set_cell(grid_size-1, i, tiles.BOARDER)
		area_filled += 4
	
	#draw starting wall beside the boarder
	for i in range(1, grid_size-1):
		level.set_cell(i, 1, tiles.WALL)
		level.set_cell(i, grid_size-2, tiles.WALL)
		level.set_cell(1, i, tiles.WALL)
		level.set_cell(grid_size-2, i, tiles.WALL)
		area_filled += 4
	
	#draw HUD outline
	for x in range(grid_size, grid_size + 8):
		for y in range(grid_size):
			boarder.set_cell(x, y, tiles.HUD)
	for x in range(grid_size + 8):
		for y in range(grid_size, grid_size + 8):
			boarder.set_cell(x, y, tiles.HUD)
	
	#draw progress bar
	for x in range(grid_size + 2, grid_size + 6):
		for y in range(3, grid_size - 3):
			boarder.set_cell(x, y, tiles.BAR)
	
	#GUI
	var shift = 1 + levels.difficulty * 8
	for x in range(shift, shift + 5):
		for y in range(grid_size, grid_size + 8):
			boarder.set_cell(x, y, tiles.BROWN)


func update_fill_precentage():
	#find the percent of that area that's still empty
	#then remove that much from the progress bar
	#note that the bar shrinks from both sides
	var percent = float(area_filled) / area_total
	
	#the bar isn't a real progress bar like you find in GUIs
	#so magic numbers are semi-required ;)
	var x = int(percent*(40.0 + levels.mob_count[levels.current_level] * levels.area_per_enemy[levels.difficulty])) 
	var n = x - bar_tiles_filled
	fill_bar_by(n)
	
	#the goal percentage scales depending on the number of mobs and the levels.difficulty setting
	var goal_percentage = 100.0 - levels.mob_count[levels.current_level] * levels.area_per_enemy[levels.difficulty]
	if(percent * 100.0 >= goal_percentage):
		on_level_complete()


func fill_bar_by(n):
	#fill bar from both sides
	#start from where both ends currently are and move inward
	var start1 = 3 + bar_tiles_filled
	var end1 = start1 + n
	var end2 = grid_size - 3 - bar_tiles_filled
	var start2 = end2 - n
	bar_tiles_filled += n
	for x in range(grid_size + 2, grid_size + 6):
		for y in range(start1, end1):
			boarder.set_cell(x, y, tiles.HUD)
		for y in range(start2, end2):
			boarder.set_cell(x, y, tiles.HUD)


func load_map():
	#draw an empty map
	initialize_map()
	
	#spawn mobs
	for i in range(levels.mob_count[levels.current_level]):
		var node = null
		var mob = levels.mob_types[levels.current_level][i]
		if(mob == levels.MOB):
			node = levels.mob_scene.instance()
		elif(mob == levels.BLAZER):
			node = levels.blazer_scene.instance()
		elif(mob == levels.GHOST):
			node = levels.ghost_scene.instance()
		elif(mob == levels.BOULDER):
			node = levels.boulder_scene.instance()
		elif(mob == levels.CRUMBLER):
			node = levels.crumbler_scene.instance()
		elif(mob == levels.ALGAE):
			node = levels.algae_scene.instance()
		else:
			print("error")
			break
		
		node.set_pos(levels.mob_pos[levels.current_level][i])
		mobs.add_child(node)


func on_level_complete():
	anim.play("switch_level")


func switch_level():
	levels.switch_level(1)
