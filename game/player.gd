extends KinematicBody2D

var speed = 50.0

var tile = Vector2()
var tile_type = 0
var line_tile_type = 0
var line_tiles = []

const VERTICAL = 0
const HORIZONTAL = 1
var direction = VERTICAL

const DIR_UP = Vector2(0, -1)
const DIR_DOWN = Vector2(0, 1)
const DIR_LEFT = Vector2(-1, 0)
const DIR_RIGHT = Vector2(1, 0)

const ROT_UP = deg2rad(0)
const ROT_DOWN = deg2rad(180)
const ROT_LEFT = deg2rad(90)
const ROT_RIGHT = deg2rad(270)

onready var root = get_tree().get_root().get_node("Game")
onready var level = get_tree().get_root().get_node("Game/Level")
onready var line = get_tree().get_root().get_node("Game/Line")
onready var boarder = get_tree().get_root().get_node("Game/Boarder")
onready var mobs = get_tree().get_root().get_node("Game/Mobs")
onready var anim = get_tree().get_root().get_node("Game/AnimationPlayer")
onready var music = get_tree().get_root().get_node("/root/music_player")
onready var sound = get_node("SamplePlayer")

func _ready():
	set_fixed_process(true)
	music.switch_to_passive()


func _fixed_process(delta):
	#figure out which tile player is on
	tile_type = get_tile_type()
	line_tile_type = get_line_tile_type()
	
	if(tile_type == tiles.WALL):
		music.switch_to_passive()
		
		if(line_tiles.size() == 1):
			if(direction == VERTICAL):
				if(level.get_cell(line_tiles[0].x,line_tiles[0].y-1)==tiles.WALL and level.get_cell(line_tiles[0].x,line_tiles[0].y+1)==tiles.WALL):
					fill()
			else:
				if(level.get_cell(line_tiles[0].x-1,line_tiles[0].y)==tiles.WALL and level.get_cell(line_tiles[0].x+1,line_tiles[0].y)==tiles.WALL):
					fill()
		
		#if a line is completed, fill the line and the empty area
		if(line_tiles.size() > 1):
			fill()
		
		#erase remaining line after returning to a wall
		elif(line_tiles.size() > 0):
			line.set_cellv(line_tiles[-1], tiles.EMPTY)
			line_tiles.pop_back()
		
		#move freely along walls
		if(Input.is_action_pressed("player_up")):
			direction = VERTICAL
			moveAndSlide(DIR_UP*speed*delta)
			set_rot(ROT_UP)
		elif(Input.is_action_pressed("player_down")):
			direction = VERTICAL
			moveAndSlide(DIR_DOWN*speed*delta)
			set_rot(ROT_DOWN)
		elif(Input.is_action_pressed("player_left")):
			direction = HORIZONTAL
			moveAndSlide(DIR_LEFT*speed*delta)
			set_rot(ROT_LEFT)
		elif(Input.is_action_pressed("player_right")):
			direction = HORIZONTAL
			moveAndSlide(DIR_RIGHT*speed*delta)
			set_rot(ROT_RIGHT)
	
	if(line_tile_type == tiles.EMPTY and tile_type == tiles.EMPTY):
		music.switch_to_active()
		
		#draw line on empty tiles
		var tile = get_tile()
		line.set_cellv(tile, tiles.LINE)
		line_tile_type = tiles.LINE
		line_tiles.append(tile)
		
	if(line_tile_type == tiles.LINE):
		#erase line when movng backwards along line
		if(line_tiles.size() > 0):
			if(get_tile() != line_tiles[-1]):
				line.set_cellv(line_tiles[-1], tiles.EMPTY)
				line_tiles.pop_back()
		
		#move only on the axis of the line
		if(direction == VERTICAL):
			if(Input.is_action_pressed("player_up")):
				moveAndSlide(DIR_UP*speed*delta)
				set_rot(ROT_UP)
			elif(Input.is_action_pressed("player_down")):
				moveAndSlide(DIR_DOWN*speed*delta)
				set_rot(ROT_DOWN)
		else:
			if(Input.is_action_pressed("player_left")):
				moveAndSlide(DIR_LEFT*speed*delta)
				set_rot(ROT_LEFT)
			elif(Input.is_action_pressed("player_right")):
				moveAndSlide(DIR_RIGHT*speed*delta)
				set_rot(ROT_RIGHT)


#move and slide along collision normal if the motion gets blocked
func moveAndSlide(motion):
	#snap movement to the middle of the tile
	if(direction == VERTICAL):
		motion += Vector2(level.map_to_world(get_tile()).x - get_pos().x + 1, 0)
	else:
		motion += Vector2(0, level.map_to_world(get_tile()).y - get_pos().y + 1)
		
	#keep remaining motion after a collision
	motion = move(motion)
	
	if(is_colliding()):
		#var collider = get_collider()
		
		#slide along the collision normal
		var n = get_collision_normal()
		motion = n.slide(motion)
		move(motion)


func get_tile():
	return level.world_to_map(get_pos())


func get_tile_type():
	return level.get_cellv(get_tile())


func get_line_tile_type():
	return line.get_cellv(get_tile())


func fill():
	music.switch_to_passive()
	
	#convert line tiles to wall tiles
	for i in line_tiles:
		level.set_cellv(i, tiles.WALL)
		count_fill()
		line.set_cellv(i, tiles.EMPTY)
	
	#overly complicated code at is basically a bunch of checks to find switch side of the division is empty
	#if one side is empty, it calles fill_area() on it
	#if no side is empty (a mob on both sides), then both sides's areas remain empty
	if(direction == VERTICAL):
		#check for mobs on the left
		var is_left_empty = true
		for mob in mobs.get_children():
			var mob_pos = level.world_to_map(mob.get_pos())
			if(mob_pos.x < line_tiles[0].x):
				#look for a line on the way there
				var found_line = false
				for i in range(1, line_tiles[0].x - mob_pos.x - 1):
					if(level.get_cell(line_tiles[0].x - i, line_tiles[0].y) == tiles.WALL):
						found_line = true
						break
				if(found_line):
					#move on to the next mob since a mob behind a line is irrelevant
					continue
				if(line_tiles[0].y < line_tiles[-1].y):
					if(mob_pos.y > line_tiles[0].y and mob_pos.y < line_tiles[-1].y):
						is_left_empty = false
						break
				else:
					if(mob_pos.y < line_tiles[0].y and mob_pos.y > line_tiles[-1].y):
						is_left_empty = false
						break
		if(is_left_empty):
			#fill left
			fill_area(Vector2(line_tiles[-1].x - 1, line_tiles[-1].y), line_tiles.size())
		else:
			#check for mobs on the right if the left wasn't empty
			var is_right_empty = true
			for mob in mobs.get_children():
				var mob_pos = level.world_to_map(mob.get_pos())
				if(mob_pos.x > line_tiles[0].x):
					#look for a line on the way there
					var found_line = false
					for i in range(1, mob_pos.x - line_tiles[0].x - 1):
						if(level.get_cell(line_tiles[0].x + i, line_tiles[0].y) == tiles.WALL):
							found_line = true
							break
					if(found_line):
						#move on to the next mob since a mob behind a line is irrelevant
						continue
					if(line_tiles[0].y < line_tiles[-1].y):
						if(mob_pos.y > line_tiles[0].y and mob_pos.y < line_tiles[-1].y):
							is_right_empty = false
							break
					else:
						if(mob_pos.y < line_tiles[0].y and mob_pos.y > line_tiles[-1].y):
							is_right_empty = false
							break
			if(is_right_empty):
				#fill right
				fill_area(Vector2(line_tiles[-1].x + 1, line_tiles[-1].y), line_tiles.size())
	else:
		#check for mobs on the top
		var is_top_empty = true
		for mob in mobs.get_children():
			var mob_pos = level.world_to_map(mob.get_pos())
			if(mob_pos.y < line_tiles[0].y):
				#look for a line on the way there
				var found_line = false
				for i in range(1, line_tiles[0].y - mob_pos.y - 1):
					if(level.get_cell(line_tiles[0].x, line_tiles[0].y - i) == tiles.WALL):
						found_line = true
						break
				if(found_line):
					#move on to the next mob since a mob behind a line is irrelevant
					continue
				if(line_tiles[0].x < line_tiles[-1].x):
					if(mob_pos.x > line_tiles[0].x and mob_pos.x < line_tiles[-1].x):
						is_top_empty = false
						break
				else:
					if(mob_pos.x < line_tiles[0].x and mob_pos.x > line_tiles[-1].x):
						is_top_empty = false
						break
		if(is_top_empty):
			#fill top
			fill_area(Vector2(line_tiles[-1].x, line_tiles[-1].y - 1), line_tiles.size())
		else:
			#check for mobs on the right if the top wasn't empty
			var is_bottom_empty = true
			for mob in mobs.get_children():
				var mob_pos = level.world_to_map(mob.get_pos())
				if(mob_pos.y > line_tiles[0].y):
					#look for a line on the way there
					var found_line = false
					for i in range(1, mob_pos.y - line_tiles[0].y - 1):
						if(level.get_cell(line_tiles[0].x, line_tiles[0].y + i) == tiles.WALL):
							found_line = true
							break
					if(found_line):
						#move on to the next mob since a mob behind a line is irrelevant
						continue
					if(line_tiles[0].x < line_tiles[-1].x):
						if(mob_pos.x > line_tiles[0].x and mob_pos.x < line_tiles[-1].x):
							is_bottom_empty = false
							break
					else:
						if(mob_pos.x < line_tiles[0].x and mob_pos.x > line_tiles[-1].x):
							is_bottom_empty = false
							break
			if(is_bottom_empty):
				#fill bottom
				fill_area(Vector2(line_tiles[-1].x, line_tiles[-1].y + 1), line_tiles.size())
	
	#clear the line tile list after it's done using it
	line_tiles.clear()
	
	#update HUD
	update_fill_precentage()


func fill_area(head, size):
	#fill in all the tiles in a open area that contains the head tile
	#uses the size of the line to optimize performance a little
	var tile = get_tile()
	if(direction == VERTICAL):
		#is head on the left or the right of player?
		#used to figure out which area to fill
		if(head.x < tile.x):
			#is head above or below player?
			#used to figure out which direction to fill from
			if(head.y < tile.y):
				#
				var x = 0
				while(level.get_cell(head.x - x, head.y) != tiles.WALL):
					for y in range(size):
						boarder.set_cell(head.x - x, head.y - y, tiles.FILLED)
						count_fill()
					x += 1
			else:
				var x = 0
				while(level.get_cell(head.x - x, head.y) != tiles.WALL):
					for y in range(size):
						boarder.set_cell(head.x - x, head.y + y, tiles.FILLED)
						count_fill()
					x += 1
		else:
			if(head.y < tile.y):
				var x = 0
				while(level.get_cell(head.x + x, head.y) != tiles.WALL):
					for y in range(size):
						boarder.set_cell(head.x + x, head.y - y, tiles.FILLED)
						count_fill()
					x += 1
			else:
				var x = 0
				while(level.get_cell(head.x + x, head.y) != tiles.WALL):
					for y in range(size):
						boarder.set_cell(head.x + x, head.y + y, tiles.FILLED)
						count_fill()
					x += 1
	#flip the logic if the line was horizontal
	else:
		if(head.y < tile.y):
			if(head.x < tile.x):
				var y = 0
				while(level.get_cell(head.x, head.y - y) != tiles.WALL):
					for x in range(size):
						boarder.set_cell(head.x - x, head.y - y, tiles.FILLED)
						count_fill()
					y += 1
			else:
				var y = 0
				while(level.get_cell(head.x, head.y - y) != tiles.WALL):
					for x in range(size):
						boarder.set_cell(head.x + x, head.y - y, tiles.FILLED)
						count_fill()
					y += 1
		else:
			if(head.x < tile.x):
				var y = 0
				while(level.get_cell(head.x, head.y + y) != tiles.WALL):
					for x in range(size):
						boarder.set_cell(head.x - x, head.y + y, tiles.FILLED)
						count_fill()
					y += 1
			else:
				var y = 0
				while(level.get_cell(head.x, head.y + y) != tiles.WALL):
					for x in range(size):
						boarder.set_cell(head.x + x, head.y + y, tiles.FILLED)
						count_fill()
					y += 1


func count_fill():
	root.area_filled += 1


func update_fill_precentage():
	root.update_fill_precentage()


func hit():
	speed = 0.0
	sound.play("explode")
	anim.play("death")

func restart():
	#todo: play dying anim and have it trigger the restart
	#restart level when hit
	levels.reload()
