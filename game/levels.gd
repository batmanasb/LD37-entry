extends Node

#mob types
const MOB = 0
const BLAZER = 1
const GHOST = 2
const BOULDER = 3
const CRUMBLER = 4
const ALGAE = 5

#positions
const START = 30
const MID = 101
const END = 170

#spawn points
var TOP_LEFT = Vector2(START, START)
var TOP_MID = Vector2(MID, START)
var TOP_RIGHT = Vector2(END, START)
var MID_LEFT = Vector2(START, MID)
var MID_MID = Vector2(MID, MID)
var MID_RIGHT = Vector2(END, START)
var BOT_LEFT = Vector2(START, END)
var BOT_MID = Vector2(MID, END)
var BOT_RIGHT = Vector2(END, END)

var mob_count = []
var mob_pos = []
var mob_types = []


var max_level = -1
var current_level = 0
var main_scene = preload("res://main.tscn")
var menu_scene = preload("res://menu.tscn")
var end_scene = preload("res://end.tscn")
var mob_scene = preload("res://mob.tscn")
var algae_scene = preload("res://algae.tscn")
var blazer_scene = preload("res://blazer.tscn")
var boulder_scene = preload("res://boulder.tscn")
var crumbler_scene = preload("res://crumbler.tscn")
var ghost_scene = preload("res://ghost.tscn")

onready var music = get_tree().get_root().get_node("/root/music_player")


const EASY = 0
const NORMAL = 1
const HARD = 2
var difficulty = NORMAL
const area_per_enemy = [5.0, 3.5, 2.2] #per difficulty

func _ready():
	set_process_input(true)
	
	#configure game window
	OS.set_window_position(Vector2(300,50))
	OS.set_window_size(Vector2(800,800))
	
	#define levels
	
	#levels
	add_level([MOB], [MID_MID])
	add_level([MOB, MOB], [MID_LEFT, MID_RIGHT])
	add_level([BLAZER], [MID_MID])
	add_level([BLAZER, MOB, MOB], [MID_LEFT, MID_RIGHT, MID_MID])
	add_level([MOB, MOB, MOB, MOB], [MID_LEFT, MID_RIGHT, MID_MID, TOP_MID])
	add_level([MOB, GHOST], [MID_LEFT, MID_RIGHT])
	add_level([MOB, MOB, GHOST], [MID_LEFT, MID_RIGHT, MID_MID])
	add_level([BLAZER, MOB, GHOST], [MID_LEFT, MID_RIGHT, MID_MID])
	add_level([MOB, BOULDER, BLAZER], [TOP_LEFT, MID_MID, BOT_RIGHT])
	add_level([BOULDER, BOULDER], [TOP_MID, BOT_MID])
	add_level([BOULDER, CRUMBLER], [TOP_LEFT, BOT_LEFT])
	add_level([ALGAE, MOB, MOB, MOB, MOB], [MID_MID, TOP_LEFT, BOT_LEFT, TOP_RIGHT, BOT_RIGHT])
	add_level([ALGAE, ALGAE, ALGAE], [TOP_LEFT, BOT_MID, TOP_RIGHT])
	add_level([BOULDER, CRUMBLER, CRUMBLER, MOB], [BOT_MID, TOP_LEFT, TOP_MID, TOP_RIGHT])
	add_level([GHOST, GHOST, GHOST], [BOT_LEFT, TOP_MID, BOT_RIGHT])
	add_level([BOULDER, CRUMBLER, GHOST], [MID_LEFT, MID_RIGHT, MID_MID])
	add_level([ALGAE, ALGAE, ALGAE, ALGAE, BLAZER], [TOP_LEFT, BOT_LEFT, TOP_RIGHT, BOT_RIGHT, MID_MID])
	add_level([BLAZER, BLAZER, BLAZER],[MID_LEFT, MID_MID, MID_RIGHT])
	add_level([ALGAE, ALGAE, ALGAE, ALGAE, ALGAE, ALGAE], [TOP_LEFT, TOP_MID, TOP_RIGHT, BOT_LEFT, BOT_RIGHT, BOT_MID])
	add_level([BOULDER, CRUMBLER, CRUMBLER, BLAZER], [MID_MID, TOP_LEFT, TOP_RIGHT, BOT_MID])
	add_level([GHOST, ALGAE, ALGAE, BLAZER], [TOP_LEFT, BOT_LEFT, MID_LEFT, MID_RIGHT])
	add_level([BOULDER, CRUMBLER, CRUMBLER, CRUMBLER], [TOP_LEFT, BOT_MID, BOT_RIGHT, MID_RIGHT])
	add_level([BLAZER, BLAZER, BLAZER, BLAZER], [TOP_LEFT, BOT_MID, BOT_RIGHT, MID_RIGHT])
	add_level([MOB, BLAZER, BOULDER, CRUMBLER, GHOST, ALGAE, ALGAE], [BOT_LEFT, BOT_RIGHT, BOT_MID, TOP_LEFT, TOP_RIGHT, TOP_MID, MID_MID])
	
	#figure out max level
	max_level = levels.mob_count.size() - 1


func add_level(types, pos):
	mob_count.append(types.size())
	mob_types.append(types)
	mob_pos.append(pos)


func _input(event):
	if(Input.is_action_pressed("skip_forward")):
		switch_level(1)
	elif(Input.is_action_pressed("go_back")):
		switch_level(-1)
	elif(Input.is_action_pressed("restart")):
		reload()
	elif(Input.is_action_pressed("difficulty_easy")):
		difficulty = EASY
		reload()
	elif(Input.is_action_pressed("difficulty_normal")):
		difficulty = NORMAL
		reload()
	elif(Input.is_action_pressed("difficulty_hard")):
		difficulty = HARD
		reload()
	elif(Input.is_action_pressed("escape")):
		get_tree().change_scene_to(menu_scene)
		music.switch_to_menu()

func reload():
	get_tree().change_scene_to(main_scene)


func switch_level(n):
	current_level += n
	if(current_level > max_level):
		get_tree().change_scene_to(end_scene)
		current_level = 0
		return
	elif(current_level < 0):
		current_level = max_level
	reload()