Batmanasb's LD37 entry

Name: Room & Gloom
Engine: Godot 2.1.1
Platforms: Linux, Windows, Mac
License: MIT (https://opensource.org/licenses/MIT)

Objective: 
Fill in enough room without getting getting hit in the process 

Controls: 
*WASD/Arrows for movement 
*R/Backspace to start/restart 
*1/2/3 to switch difficulty (see bottom left of HUD) 
*The minus and plus keys near backspace let you skip levels

Music Tracks:
*Pumped
*Sea Battles in Space 
*Create A Song in A Day Challenge #1

All music tracks are by RoccoW is licensed under Attribution 4.0 International (CC BY 4.0)
(https://creativecommons.org/licenses/by/4.0/)


